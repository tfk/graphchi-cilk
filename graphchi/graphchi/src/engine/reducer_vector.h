// -*- C++ -*-
// Copyright (c) 2010, Tao B. Schardl
/*
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
*/

#ifndef REDUCER_VECTOR_H
#define REDUCER_VECTOR_H

#include <stdlib.h>
#include <assert.h>
#include <cilk/cilk.h>
#include <cilk/reducer.h>

#include <cilk/cilk_api.h>

template <typename T> class Vector_reducer;

template <typename T>
class Vector_wrapper
{
  public:
    std::vector<T>** vectors;
    Vector_wrapper();
    void set_vectors(std::vector<T>* init);
};

template <typename T>
void Vector_wrapper<T>::set_vectors(std::vector<T>* init) {
  vectors = init;
}

template <typename T>
Vector_wrapper<T>::Vector_wrapper() {
  vectors = NULL;
}

template <typename T>
class Vector_reducer
{
public:
  struct Monoid: cilk::monoid_base< Vector_wrapper<T> >
  {
    static void reduce (Vector_wrapper<T> *left, Vector_wrapper<T> *right) {
      if (left->vectors == NULL && right->vectors == NULL) {
        printf("Error! Error! Both vector lists are null\n");
        return;
      }
      if (left->vectors == NULL) {
        *left = *right;
      }
    }
  };
private:
  cilk::reducer<Monoid> imp_;
public:
  Vector_reducer();
  void insert(T);
  inline std::vector<T> &get_reference();
  inline uint numElements() const;
  inline bool isEmpty() const;
  inline void clear();
  std::vector<T>** vectors;
};

template <typename T>
Vector_reducer<T>::Vector_reducer() {
  vectors = (std::vector<T>**) malloc(sizeof(std::vector<T>*)*12);
  for (int i = 0; i < 12; i++) {
    vectors[i] = NULL;
  }
}

template <typename T>
void
Vector_reducer<T>::insert(T el)
{
  // TODO(TFK): 15% of serial time is due to this call.
  int wid = __cilkrts_get_worker_number();
  //int wid = 0;
  if (vectors[wid] == NULL) {
    vectors[wid] = new std::vector<T>();
  }
  vectors[wid]->push_back(el);
}

template <typename T>
inline std::vector<T>&
Vector_reducer<T>::get_reference()
{
  int* sizes = (int*) malloc(sizeof(int)*12);
  sizes[0] = 0;
  int total_size = vectors[0]->size();
  printf("worker %d has size %d\n", 0, (int)vectors[0]->size());
  for (int i = 1; i < 12; i++) {
    if (vectors[i] == NULL) break;
    sizes[i] = sizes[i-1] + vectors[i-1]->size();
    total_size += vectors[i]->size();
    printf("worker %d has size %d\n", i, (int)vectors[i]->size());
  }
  printf("total size is %d\n", total_size);
  vectors[0]->resize(total_size);
  for (int i = 1; i < 12; i++) {
    if (vectors[i] == NULL) {
      continue;
    }
    cilk_for (int j = 0; j < vectors[i]->size(); j++) {
      (*vectors[0])[sizes[i] + j] = (*vectors[i])[j];
    }
  }
  return *vectors[0];
}

template <typename T>
inline uint
Vector_reducer<T>::numElements() const
{
  return vectors[0]->size();
}

template <typename T>
inline bool
Vector_reducer<T>::isEmpty() const
{
  return vectors[0]->size() == 0;
}

template <typename T>
inline void
Vector_reducer<T>::clear()
{
  for (int i = 0; i < 12; i++) {
    if (vectors[i] == NULL) {
      continue;
    }
    vectors[i]->clear();
  }
}

#include "reducer_vector.cpp"

#endif
